// IMPORTS PACKAGES
import { Db, Filter, FindOptions, MongoClient, ObjectId } from 'mongodb';

// IMPORTS PROJECT
import { environmentVariables } from '../config/index';

// ENCODE COMPONENTS
const USER = encodeURIComponent(environmentVariables.dbUser!);
const PASSWORD = encodeURIComponent(environmentVariables.dbPassword!);
const DB_NAME = environmentVariables.dbName;

// MONGODB CONNECT URL
const MONGO_URI = `mongodb+srv://${USER}:${PASSWORD}@${environmentVariables.dbHost}/${DB_NAME}?retryWrites=true&w=majority`;

export default class MongoLib {
  client: MongoClient;
  dbName: string;
  constructor() {
    this.client = new MongoClient(MONGO_URI, {
      heartbeatFrequencyMS: 30000,
      keepAlive: true,
    });
    this.dbName = DB_NAME!;
  }

  // ESTABLISH CONNECTION
  private connect(): Promise<Db> | undefined {
    return new Promise((resolve, reject) => {
      this.client.connect((err) => {
        if (err) reject(err);
        console.log('Connecting successfully to mongo!');
        resolve(this.client.db(this.dbName));
      });
    });
  }

  // GET MANY DOCUMENTS
  public getAll(collection: string, query: Filter<any>) {
    const data = this.connect()?.then((db) =>
      db.collection(collection).find(query).sort({ $natural: -1 }).toArray()
    );
    return data;
  }

  // GET A DOCUMENT
  public get(collection: string, id: string) {
    return this.connect()?.then((db) =>
      db.collection(collection).findOne({ _id: new ObjectId(id) })
    );
  }

  // CREATE A DOCUMENT
  public create(collection: string, data: object) {
    return this.connect()?.then((db) =>
      db
        .collection(collection)
        .insertOne(data)
        .then((result) => result.insertedId)
    );
  }

  // UPDATE A DOCUMENT
  public update(collection: string, id: string, data: object) {
    return this.connect()?.then((db) =>
      db
        .collection(collection)
        .updateOne({ _id: new ObjectId(id) }, { $set: data }, { upsert: true })
        .then((result) => result)
    );
  }

  // DELETE A DOCUMENT
  public delete(collection: string, id: string) {
    return this.connect()?.then((db) =>
      db
        .collection(collection)
        .deleteOne({ _id: new ObjectId(id) })
        .then(() => id)
    );
  }
}
