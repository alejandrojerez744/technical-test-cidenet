// IMPORTS PACKAGES
import joi from 'joi';

export const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

export const updateUserSchema = {
  data: joi
    .object({
      surname: joi
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .strict(),
      secondSurname: joi
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .strict(),
      firstName: joi
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .strict(),
      otherName: joi
        .string()
        .regex(/^[A-Z\s]+$/)
        .uppercase()
        .normalize('NFD')
        .max(50)
        .strict(),
      country: joi.string().valid('co', 'us'),
      idType: joi
        .string()
        .valid(
          'cedula de ciudadania',
          'cedula de extranjeria',
          'pasaporte',
          'permiso especial'
        ),
      id: joi.string().alphanum(),
      area: joi
        .string()
        .valid(
          'administracion',
          'financiera',
          'compras',
          'infraestructura',
          'operacion',
          'talento humano',
          'servicios varios'
        ),
      otherArea: joi.string().optional(),
      state: joi.string().default('activo'),
      password: joi.string().max(20).min(6),
    })
    .required(),
  _id: userIdSchema.required(),
};

export const createUserSchema = {
  surname: joi
    .string()
    .regex(/^[A-Z]+$/)
    .uppercase()
    .normalize('NFD')
    .max(20)
    .required()
    .strict(),
  secondSurname: joi
    .string()
    .regex(/^[A-Z]+$/)
    .uppercase()
    .normalize('NFD')
    .max(20)
    .required()
    .strict(),
  firstName: joi
    .string()
    .regex(/^[A-Z]+$/)
    .uppercase()
    .normalize('NFD')
    .max(20)
    .required()
    .strict(),
  otherName: joi
    .string()
    .regex(/^[A-Z\s]+$/)
    .uppercase()
    .normalize('NFD')
    .max(50)
    .required()
    .strict(),
  country: joi.string().required().valid('co', 'us'),
  idType: joi
    .string()
    .required()
    .valid(
      'cedula de ciudadania',
      'cedula de extranjeria',
      'pasaporte',
      'permiso especial'
    ),
  id: joi.string().required().alphanum(),
  dateAdmission: joi
    .date()
    .max(Date.now())
    .min(new Date().setMonth(new Date().getMonth() - 1))
    .required(),
  area: joi
    .string()
    .valid(
      'administracion',
      'financiera',
      'compras',
      'infraestructura',
      'operacion',
      'talento humano',
      'servicios varios'
    )
    .required(),
  otherArea: joi.string().optional(),
  state: joi.string().default('activo').required(),
  password: joi.string().max(20).min(6).required(),
};
