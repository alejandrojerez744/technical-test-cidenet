// IMPORTS PACKAGES
import passport from 'passport';
import { BasicStrategy } from 'passport-http';
import boom from '@hapi/boom';
import bcrypt from 'bcrypt';

// IMPORTS PROJECT
import AuthService from '../../services/auth.service';

passport.use(
  new BasicStrategy(async (email: string, password: string, cb: Function) => {
    try {
      const authService = new AuthService();
      const user = await authService.getUser({ email });
      if (!user) {
        return cb(boom.unauthorized(), false);
      }

      if (!(await bcrypt.compare(password, user.password))) {
        return cb(boom.unauthorized(), false);
      }

      delete user.password;

      return cb(null, user);
    } catch (err) {
      return cb(err);
    }
  })
);
