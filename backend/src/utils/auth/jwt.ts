// IMPORTS PACKAGES
import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import boom from '@hapi/boom';

// IMPORTS PROJECT
import AuthService from '../../services/auth.service';
import { environmentVariables } from '../../config/index';

passport.use(
  new Strategy(
    {
      secretOrKey: environmentVariables.authJwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
    async (tokenpayload: UserDB, cb) => {
      console.log(tokenpayload);
      try {
        const authService = new AuthService();

        const user = await authService.getUser({
          email: tokenpayload.email,
        });

        if (!user) return cb(boom.unauthorized(), false);

        delete user.password;

        return cb(null, user);
      } catch (err) {
        return cb(err);
      }
    }
  )
);
