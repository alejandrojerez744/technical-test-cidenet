// IMPORTS PACKAGES
import boom from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';

// IMPORTS PROJECT
import AuthService from '../../services/auth.service';

export const createEmail = async (
  firstName: string,
  surname: string,
  id: string,
  country: string,
  next: NextFunction
) => {
  const authService = new AuthService();

  let email = `${firstName}.${surname
    .split(' ')
    .join('')}@cidenet.com.${country}`;

  const emailInUse = await authService.getUser({ email });

  if (emailInUse) {
    email = `${firstName}.${surname.split(' ').join('')}.${id.substring(
      id.length - 3,
      id.length
    )}@cidenet.com.${country}`;
  }

  return email;
};

export const userVerification = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { body: user }: { body: CreateUser } = req;

  const authService = new AuthService();

  const idInUse = await authService.getAllUsers({ id: user.id });

  if (idInUse!.length > 0) {
    return next(boom.badData('id is already in use'));
  }

  const email = await createEmail(
    user.firstName,
    user.surname,
    user.id,
    user.country,
    next
  );

  /* const authService = new AuthService();

  const idInUse = await authService.getAllUsers({ id: user.id });

  if (idInUse!.length > 0) {
    return next(boom.badData('id is already in use'));
  }

  let email = `${user.firstName}.${user.surname
    .split(' ')
    .join('')}@cidenet.com.${user.country}`;

  const emailInUse = await authService.getUser({ email });

  if (emailInUse) {
    email = `${user.firstName}.${user.surname
      .split(' ')
      .join('')}.${user.id.substring(
      user.id.length - 3,
      user.id.length
    )}@cidenet.com.${user.country}`;
  } */

  req.body.email = email;

  next();
};
