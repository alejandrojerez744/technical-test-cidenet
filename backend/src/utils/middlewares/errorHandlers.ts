// IMPORTS PACKAGES
import boom, { Boom } from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';

// IMPORTS PROJECT
import { environmentVariables } from '../../config';

const withErrorStack = (err: object, stack: string | undefined) => {
  if (environmentVariables.dev) {
    return { ...err, stack };
  }

  return err;
  0;
};

const logError = (
  err: object,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log(err);
  next(err);
};

const wrapError = (
  err: Boom,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!err.isBoom) {
    next(boom.badImplementation(err.message));
  }

  next(err);
};

const errorHandler = (
  err: Boom,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    output: { statusCode, payload },
  } = err;

  res.status(statusCode).json(withErrorStack(payload, err.stack));
};

export { logError, wrapError, errorHandler };
