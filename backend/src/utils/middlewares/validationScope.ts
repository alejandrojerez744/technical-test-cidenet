import boom from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';

export const validationScope = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const user: UserDB = req.user as UserDB;
  console.log(user);
  if (!user.isAdmin) {
    next(
      boom.unauthorized('you do not have permissions to register a new user')
    );
  }

  next();
};
