// IMPORTS PACKAGES
import boom from '@hapi/boom';
import { NextFunction, Request, Response } from 'express';
import joi from 'joi';

const validate = (data: object, schema: object) => {
  const { error } = joi.object(schema).validate(data);
  return error;
};

export const validationHandler = (schema: object, check = 'body') => {
  return (req: Request, _res: Response, next: NextFunction) => {
    const dictionary: { [key: string]: any } = {
      body: req.body,
      query: req.query,
      params: req.params,
    };
    const err = validate(dictionary[check], schema);
    err ? next(boom.badRequest(err.message)) : next();
  };
};
