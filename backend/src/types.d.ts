interface EnvironmentVariables {
  dev: boolean;
  port?: string | number;
  dbUser?: string;
  dbPassword?: string;
  dbHost?: string;
  dbName?: string;
  authJwtSecret?: string;
}

type GetUser = { email: string };

type CreateUser = {
  surname: string;
  secondSurname: string;
  firstName: string;
  otherName: string;
  country: string;
  idType: string;
  id: string;
  email?: string;
  recordDateTime?: string;
  dateAdmission: Date;
  area: string;
  otherArea: string;
  state: string;
  password: string;
};

type UpdateUser = {
  surname?: string;
  secondSurname?: string;
  firstName?: string;
  otherName?: string;
  country?: string;
  idType?: string;
  id?: string;
  email?: string;
  recordDateTime?: string;
  dateAdmission?: Date;
  area?: string;
  otherArea?: string;
  state?: string;
  password?: string;
};

type DeleteUser = { id: string };

interface UserDB extends CreateUser {
  _id: string;
  password?: string;
  email: string;
  isAdmin?: boolean;
}
