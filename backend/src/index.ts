// IMPORTS PACKAGES
import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';

// IMPORTS PROJECT
import { environmentVariables } from './config';
import {
  errorHandler,
  logError,
  wrapError,
} from './utils/middlewares/errorHandlers';
import { notFoundHandler } from './utils/middlewares/notFoundHandler';
import { authApi } from './routes/auth.routes';
import { usersApi } from './routes/users.routes';

const app = express();

// MIDDLEWARES
app.use(express.json());
app.use(morgan('dev'));
app.use(helmet());
app.use(cors());

// ROUTES
authApi(app);
usersApi(app);

// MIDDLEWARE NOT FOUND
app.use(notFoundHandler);

// ERRORS MIDDLEWARES
app.use(logError);
app.use(wrapError);
app.use(errorHandler);

// INITIALIZE SERVER
app.listen(environmentVariables.port, () => {
  console.log('Listening http://localhost:3001');
});
