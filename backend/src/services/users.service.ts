// IMPORTS PROJECT
import { NextFunction } from 'express';
import MongoLib from '../lib/mongo';
import { formatDateNow } from '../utils/helpers';
import { createEmail } from '../utils/middlewares/userVerification';

export default class UsersServices {
  collection: string;
  mongoDB: MongoLib;
  constructor() {
    this.collection = 'users';
    this.mongoDB = new MongoLib();
  }

  async getAll() {
    const users = this.mongoDB.getAll(this.collection, {});
    return users;
  }

  async update(id: string, data: UpdateUser, next?: NextFunction) {
    if (data.firstName || data.surname || data.country || data.id) {
      const user = await this.mongoDB.get(this.collection, id);
      const email = await createEmail(
        data.firstName ?? user?.firstName,
        data.surname ?? user?.surname,
        data.id ?? user?.id,
        data.country ?? user?.country,
        next!
      );
      const updateUser = await this.mongoDB.update(this.collection, id, {
        ...data,
        updateDate: formatDateNow(),
        email,
      });
      return updateUser;
    } else {
      const updateUser = await this.mongoDB.update(this.collection, id, {
        ...data,
        updateDate: formatDateNow(),
      });
      return updateUser;
    }
  }

  async delete(id: string) {
    const deletedUser = await this.mongoDB.delete(this.collection, id);
    return deletedUser;
  }
}
