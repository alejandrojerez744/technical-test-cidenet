//IMPORT PACKAGES
import bcrypt from 'bcrypt';

// IMPORTS PROJECT
import MongoLib from '../lib/mongo';

export default class AuthService {
  collection: string;
  mongoDB: MongoLib;
  constructor() {
    this.collection = 'users';
    this.mongoDB = new MongoLib();
  }

  async getUser({ email }: GetUser) {
    const [user] = await this.mongoDB.getAll(this.collection, { email })!;
    return user;
  }

  async getAllUsers(query: object) {
    const users = await this.mongoDB.getAll(this.collection, query);
    return users;
  }

  async createUser(user: CreateUser) {
    const hashedPassword = await bcrypt.hash(user.password, 10);
    user.password = hashedPassword;
    const createdUser = await this.mongoDB.create(this.collection, user);
    return createdUser;
  }
}
