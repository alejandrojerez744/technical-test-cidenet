// IMPORTS PACKAGES
import { Express, Router } from 'express';
import passport from 'passport';
import boom from '@hapi/boom';
import jwt from 'jsonwebtoken';

// IMPORTS PROJECT
import { environmentVariables } from '../config';
import AuthService from '../services/auth.service';
import { userVerification } from '../utils/middlewares/userVerification';
import { validationHandler } from '../utils/middlewares/validationHandler';
import { createUserSchema } from '../utils/schemas/users';
import { formatDateNow } from '../utils/helpers';
import '../utils/auth/basic';
import '../utils/auth/jwt';
import { validationScope } from '../utils/middlewares/validationScope';

export const authApi = (app: Express) => {
  const router = Router();
  app.use('/api/auth', router);

  const authService = new AuthService();

  router.post('/sign-in', async (req, res, next) => {
    passport.authenticate('basic', (err: Error, user: UserDB) => {
      try {
        if (err || !user) {
          return next(boom.unauthorized());
        }

        delete user.password;

        const token = jwt.sign(
          {
            sub: user._id,
            ...user,
          },
          environmentVariables.authJwtSecret!,
          { expiresIn: '360ms' }
        );

        res.status(200).json({
          message: 'sign-in successfully',
          data: { token },
        });
      } catch (err) {
        next(err);
      }
    })(req, res, next);
  });

  router.post(
    '/sign-up',
    //passport.authenticate('jwt', { session: false }),
    //validationScope,
    validationHandler(createUserSchema, 'body'),
    userVerification,
    async (req, res, next) => {
      try {
        const { body: user }: { body: CreateUser } = req;
        const createdUser = await authService.createUser({
          ...user,
          recordDateTime: formatDateNow(),
        });
        res.status(201).json({
          data: {
            insertedId: createdUser,
            email: user.email,
          },
        });
      } catch (err) {
        next(err);
      }
    }
  );
};
