// IMPORTS PACKAGES
import { Express, Router } from 'express';
import UsersServices from '../services/users.service';
import { validationHandler } from '../utils/middlewares/validationHandler';
import { updateUserSchema, userIdSchema } from '../utils/schemas/users';

export const usersApi = (app: Express) => {
  const router = Router();
  app.use('/api/users', router);

  const userService = new UsersServices();

  router.get('/', async (req, res, next) => {
    try {
      const users = await userService.getAll();
      users?.map((u) => {
        delete u.password;
        return u;
      });
      res.status(200).json({
        message: 'users list',
        data: users,
      });
    } catch (err) {
      next(err);
    }
  });

  router.put(
    '/',
    validationHandler(updateUserSchema),
    async (req, res, next) => {
      try {
        const { _id, data }: { _id: string; data: UpdateUser } = req.body;
        const updateUser = await userService.update(_id, data, next);
        res.status(200).json({
          message: 'updated user',
          data: updateUser,
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
    '/',
    validationHandler({ _id: userIdSchema.required() }),
    async (req, res, next) => {
      try {
        const { _id } = req.body;
        const deletedUser = await userService.delete(_id);
        res.status(200).json({
          message: 'user deleted',
          data: deletedUser,
        });
      } catch (err) {
        next(err);
      }
    }
  );
};
