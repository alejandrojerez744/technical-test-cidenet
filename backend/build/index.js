"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const morgan_1 = __importDefault(require("morgan"));
const config_1 = require("./config");
const errorHandlers_1 = require("./utils/middlewares/errorHandlers");
const notFoundHandler_1 = require("./utils/middlewares/notFoundHandler");
const auth_routes_1 = require("./routes/auth.routes");
const users_routes_1 = require("./routes/users.routes");
const app = express_1.default();
app.use(express_1.default.json());
app.use(morgan_1.default('dev'));
app.use(helmet_1.default());
app.use(cors_1.default());
auth_routes_1.authApi(app);
users_routes_1.usersApi(app);
app.use(notFoundHandler_1.notFoundHandler);
app.use(errorHandlers_1.logError);
app.use(errorHandlers_1.wrapError);
app.use(errorHandlers_1.errorHandler);
app.listen(config_1.environmentVariables.port, () => {
    console.log('Listening http://localhost:3001');
});
