"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const index_1 = require("../config/index");
const USER = encodeURIComponent(index_1.environmentVariables.dbUser);
const PASSWORD = encodeURIComponent(index_1.environmentVariables.dbPassword);
const DB_NAME = index_1.environmentVariables.dbName;
const MONGO_URI = `mongodb+srv://${USER}:${PASSWORD}@${index_1.environmentVariables.dbHost}/${DB_NAME}?retryWrites=true&w=majority`;
class MongoLib {
    constructor() {
        this.client = new mongodb_1.MongoClient(MONGO_URI, {
            heartbeatFrequencyMS: 30000,
            keepAlive: true,
        });
        this.dbName = DB_NAME;
    }
    connect() {
        return new Promise((resolve, reject) => {
            this.client.connect((err) => {
                if (err)
                    reject(err);
                console.log('Connecting successfully to mongo!');
                resolve(this.client.db(this.dbName));
            });
        });
    }
    getAll(collection, query) {
        var _a;
        const data = (_a = this.connect()) === null || _a === void 0 ? void 0 : _a.then((db) => db.collection(collection).find(query).sort({ $natural: -1 }).toArray());
        return data;
    }
    get(collection, id) {
        var _a;
        return (_a = this.connect()) === null || _a === void 0 ? void 0 : _a.then((db) => db.collection(collection).findOne({ _id: new mongodb_1.ObjectId(id) }));
    }
    create(collection, data) {
        var _a;
        return (_a = this.connect()) === null || _a === void 0 ? void 0 : _a.then((db) => db
            .collection(collection)
            .insertOne(data)
            .then((result) => result.insertedId));
    }
    update(collection, id, data) {
        var _a;
        return (_a = this.connect()) === null || _a === void 0 ? void 0 : _a.then((db) => db
            .collection(collection)
            .updateOne({ _id: new mongodb_1.ObjectId(id) }, { $set: data }, { upsert: true })
            .then((result) => result));
    }
    delete(collection, id) {
        var _a;
        return (_a = this.connect()) === null || _a === void 0 ? void 0 : _a.then((db) => db
            .collection(collection)
            .deleteOne({ _id: new mongodb_1.ObjectId(id) })
            .then(() => id));
    }
}
exports.default = MongoLib;
