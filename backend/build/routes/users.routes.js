"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const users_service_1 = __importDefault(require("../services/users.service"));
const validationHandler_1 = require("../utils/middlewares/validationHandler");
const users_1 = require("../utils/schemas/users");
exports.usersApi = (app) => {
    const router = express_1.Router();
    app.use('/api/users', router);
    const userService = new users_service_1.default();
    router.get('/', (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const users = yield userService.getAll();
            users === null || users === void 0 ? void 0 : users.map((u) => {
                delete u.password;
                return u;
            });
            res.status(200).json({
                message: 'users list',
                data: users,
            });
        }
        catch (err) {
            next(err);
        }
    }));
    router.put('/', validationHandler_1.validationHandler(users_1.updateUserSchema), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { _id, data } = req.body;
            const updateUser = yield userService.update(_id, data, next);
            res.status(200).json({
                message: 'updated user',
                data: updateUser,
            });
        }
        catch (err) {
            next(err);
        }
    }));
    router.delete('/', validationHandler_1.validationHandler({ _id: users_1.userIdSchema.required() }), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { _id } = req.body;
            const deletedUser = yield userService.delete(_id);
            res.status(200).json({
                message: 'user deleted',
                data: deletedUser,
            });
        }
        catch (err) {
            next(err);
        }
    }));
};
