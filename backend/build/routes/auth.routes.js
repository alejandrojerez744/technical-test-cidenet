"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const boom_1 = __importDefault(require("@hapi/boom"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = require("../config");
const auth_service_1 = __importDefault(require("../services/auth.service"));
const userVerification_1 = require("../utils/middlewares/userVerification");
const validationHandler_1 = require("../utils/middlewares/validationHandler");
const users_1 = require("../utils/schemas/users");
const helpers_1 = require("../utils/helpers");
require("../utils/auth/basic");
require("../utils/auth/jwt");
exports.authApi = (app) => {
    const router = express_1.Router();
    app.use('/api/auth', router);
    const authService = new auth_service_1.default();
    router.post('/sign-in', (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        passport_1.default.authenticate('basic', (err, user) => {
            try {
                if (err || !user) {
                    return next(boom_1.default.unauthorized());
                }
                delete user.password;
                const token = jsonwebtoken_1.default.sign(Object.assign({ sub: user._id }, user), config_1.environmentVariables.authJwtSecret, { expiresIn: '360ms' });
                res.status(200).json({
                    message: 'sign-in successfully',
                    data: { token },
                });
            }
            catch (err) {
                next(err);
            }
        })(req, res, next);
    }));
    router.post('/sign-up', validationHandler_1.validationHandler(users_1.createUserSchema, 'body'), userVerification_1.userVerification, (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { body: user } = req;
            const createdUser = yield authService.createUser(Object.assign(Object.assign({}, user), { recordDateTime: helpers_1.formatDateNow() }));
            res.status(201).json({
                data: {
                    insertedId: createdUser,
                    email: user.email,
                },
            });
        }
        catch (err) {
            next(err);
        }
    }));
};
