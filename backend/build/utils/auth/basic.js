"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const passport_http_1 = require("passport-http");
const boom_1 = __importDefault(require("@hapi/boom"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const auth_service_1 = __importDefault(require("../../services/auth.service"));
passport_1.default.use(new passport_http_1.BasicStrategy((email, password, cb) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const authService = new auth_service_1.default();
        const user = yield authService.getUser({ email });
        if (!user) {
            return cb(boom_1.default.unauthorized(), false);
        }
        if (!(yield bcrypt_1.default.compare(password, user.password))) {
            return cb(boom_1.default.unauthorized(), false);
        }
        delete user.password;
        return cb(null, user);
    }
    catch (err) {
        return cb(err);
    }
})));
