"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const boom_1 = __importDefault(require("@hapi/boom"));
const auth_service_1 = __importDefault(require("../../services/auth.service"));
exports.createEmail = (firstName, surname, id, country, next) => __awaiter(void 0, void 0, void 0, function* () {
    const authService = new auth_service_1.default();
    let email = `${firstName}.${surname
        .split(' ')
        .join('')}@cidenet.com.${country}`;
    const emailInUse = yield authService.getUser({ email });
    if (emailInUse) {
        email = `${firstName}.${surname.split(' ').join('')}.${id.substring(id.length - 3, id.length)}@cidenet.com.${country}`;
    }
    return email;
});
exports.userVerification = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { body: user } = req;
    const authService = new auth_service_1.default();
    const idInUse = yield authService.getAllUsers({ id: user.id });
    if (idInUse.length > 0) {
        return next(boom_1.default.badData('id is already in use'));
    }
    const email = yield exports.createEmail(user.firstName, user.surname, user.id, user.country, next);
    req.body.email = email;
    next();
});
