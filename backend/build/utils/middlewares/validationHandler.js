"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const boom_1 = __importDefault(require("@hapi/boom"));
const joi_1 = __importDefault(require("joi"));
const validate = (data, schema) => {
    const { error } = joi_1.default.object(schema).validate(data);
    return error;
};
exports.validationHandler = (schema, check = 'body') => {
    return (req, _res, next) => {
        const dictionary = {
            body: req.body,
            query: req.query,
            params: req.params,
        };
        const err = validate(dictionary[check], schema);
        err ? next(boom_1.default.badRequest(err.message)) : next();
    };
};
