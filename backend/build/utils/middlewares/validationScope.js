"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const boom_1 = __importDefault(require("@hapi/boom"));
exports.validationScope = (req, res, next) => {
    const user = req.user;
    console.log(user);
    if (!user.isAdmin) {
        next(boom_1.default.unauthorized('you do not have permissions to register a new user'));
    }
    next();
};
