"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const boom_1 = __importDefault(require("@hapi/boom"));
const config_1 = require("../../config");
const withErrorStack = (err, stack) => {
    if (config_1.environmentVariables.dev) {
        return Object.assign(Object.assign({}, err), { stack });
    }
    return err;
    0;
};
const logError = (err, req, res, next) => {
    console.log(err);
    next(err);
};
exports.logError = logError;
const wrapError = (err, req, res, next) => {
    if (!err.isBoom) {
        next(boom_1.default.badImplementation(err.message));
    }
    next(err);
};
exports.wrapError = wrapError;
const errorHandler = (err, req, res, next) => {
    const { output: { statusCode, payload }, } = err;
    res.status(statusCode).json(withErrorStack(payload, err.stack));
};
exports.errorHandler = errorHandler;
