"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
exports.userIdSchema = joi_1.default.string().regex(/^[0-9a-fA-F]{24}$/);
exports.updateUserSchema = {
    data: joi_1.default
        .object({
        surname: joi_1.default
            .string()
            .regex(/^[A-Z]+$/)
            .uppercase()
            .normalize('NFD')
            .max(20)
            .strict(),
        secondSurname: joi_1.default
            .string()
            .regex(/^[A-Z]+$/)
            .uppercase()
            .normalize('NFD')
            .max(20)
            .strict(),
        firstName: joi_1.default
            .string()
            .regex(/^[A-Z]+$/)
            .uppercase()
            .normalize('NFD')
            .max(20)
            .strict(),
        otherName: joi_1.default
            .string()
            .regex(/^[A-Z\s]+$/)
            .uppercase()
            .normalize('NFD')
            .max(50)
            .strict(),
        country: joi_1.default.string().valid('co', 'us'),
        idType: joi_1.default
            .string()
            .valid('cedula de ciudadania', 'cedula de extranjeria', 'pasaporte', 'permiso especial'),
        id: joi_1.default.string().alphanum(),
        area: joi_1.default
            .string()
            .valid('administracion', 'financiera', 'compras', 'infraestructura', 'operacion', 'talento humano', 'servicios varios'),
        otherArea: joi_1.default.string().optional(),
        state: joi_1.default.string().default('activo'),
        password: joi_1.default.string().max(20).min(6),
    })
        .required(),
    _id: exports.userIdSchema.required(),
};
exports.createUserSchema = {
    surname: joi_1.default
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .required()
        .strict(),
    secondSurname: joi_1.default
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .required()
        .strict(),
    firstName: joi_1.default
        .string()
        .regex(/^[A-Z]+$/)
        .uppercase()
        .normalize('NFD')
        .max(20)
        .required()
        .strict(),
    otherName: joi_1.default
        .string()
        .regex(/^[A-Z\s]+$/)
        .uppercase()
        .normalize('NFD')
        .max(50)
        .required()
        .strict(),
    country: joi_1.default.string().required().valid('co', 'us'),
    idType: joi_1.default
        .string()
        .required()
        .valid('cedula de ciudadania', 'cedula de extranjeria', 'pasaporte', 'permiso especial'),
    id: joi_1.default.string().required().alphanum(),
    dateAdmission: joi_1.default
        .date()
        .max(Date.now())
        .min(new Date().setMonth(new Date().getMonth() - 1))
        .required(),
    area: joi_1.default
        .string()
        .valid('administracion', 'financiera', 'compras', 'infraestructura', 'operacion', 'talento humano', 'servicios varios')
        .required(),
    otherArea: joi_1.default.string().optional(),
    state: joi_1.default.string().default('activo').required(),
    password: joi_1.default.string().max(20).min(6).required(),
};
