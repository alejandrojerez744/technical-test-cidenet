"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const mongo_1 = __importDefault(require("../lib/mongo"));
class AuthService {
    constructor() {
        this.collection = 'users';
        this.mongoDB = new mongo_1.default();
    }
    getUser({ email }) {
        return __awaiter(this, void 0, void 0, function* () {
            const [user] = yield this.mongoDB.getAll(this.collection, { email });
            return user;
        });
    }
    getAllUsers(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.mongoDB.getAll(this.collection, query);
            return users;
        });
    }
    createUser(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const hashedPassword = yield bcrypt_1.default.hash(user.password, 10);
            user.password = hashedPassword;
            const createdUser = yield this.mongoDB.create(this.collection, user);
            return createdUser;
        });
    }
}
exports.default = AuthService;
