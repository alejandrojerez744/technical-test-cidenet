"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongo_1 = __importDefault(require("../lib/mongo"));
const helpers_1 = require("../utils/helpers");
const userVerification_1 = require("../utils/middlewares/userVerification");
class UsersServices {
    constructor() {
        this.collection = 'users';
        this.mongoDB = new mongo_1.default();
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const users = this.mongoDB.getAll(this.collection, {});
            return users;
        });
    }
    update(id, data, next) {
        var _a, _b, _c, _d;
        return __awaiter(this, void 0, void 0, function* () {
            if (data.firstName || data.surname || data.country || data.id) {
                const user = yield this.mongoDB.get(this.collection, id);
                const email = yield userVerification_1.createEmail((_a = data.firstName) !== null && _a !== void 0 ? _a : user === null || user === void 0 ? void 0 : user.firstName, (_b = data.surname) !== null && _b !== void 0 ? _b : user === null || user === void 0 ? void 0 : user.surname, (_c = data.id) !== null && _c !== void 0 ? _c : user === null || user === void 0 ? void 0 : user.id, (_d = data.country) !== null && _d !== void 0 ? _d : user === null || user === void 0 ? void 0 : user.country, next);
                const updateUser = yield this.mongoDB.update(this.collection, id, Object.assign(Object.assign({}, data), { updateDate: helpers_1.formatDateNow(), email }));
                return updateUser;
            }
            else {
                const updateUser = yield this.mongoDB.update(this.collection, id, Object.assign(Object.assign({}, data), { updateDate: helpers_1.formatDateNow() }));
                return updateUser;
            }
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedUser = yield this.mongoDB.delete(this.collection, id);
            return deletedUser;
        });
    }
}
exports.default = UsersServices;
