import { node } from "prop-types";
import { ReactNode, useEffect } from "react";
import { useLocation } from "react-router-dom";

type Props = { children: ReactNode };

const NavigationScroll: React.FC<Props> = ({ children }) => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }, [pathname]);

  return <>{children}</>;
};

NavigationScroll.prototype = {
  children: node,
};

export default NavigationScroll;
