import { Box } from "@mui/system";
import { Outlet } from "react-router-dom";

const BasicLayout = () => {
  return (
    <Box>
      <Outlet />
    </Box>
  );
};

export default BasicLayout;
