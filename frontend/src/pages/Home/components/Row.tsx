import {
  Collapse,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  TableCell,
  TableRow,
  Tooltip,
} from "@mui/material";
import { useState } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { createData } from "./createData";
import MainCard from "../../../components/MainCard";
import { DoDisturb, ModeEdit } from "@mui/icons-material";
import { Formik } from "formik";
import * as Yup from "yup";
import { DateMask, NamesMask } from "./masks";

const Row = (props: { row: ReturnType<typeof createData> }) => {
  const { row } = props;
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState(false);

  return (
    <>
      <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.firstName}
        </TableCell>
        <TableCell>{row.surname}</TableCell>
        <TableCell>{row.area}</TableCell>
        <TableCell>{row.state}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <MainCard
              sx={{ m: 3 }}
              title="Informacion del empleado"
              secondary={
                <>
                  <Tooltip title={edit ? "Cancelar" : "Editar"}>
                    <IconButton
                      onClick={() => setEdit(!edit)}
                      color={edit ? "error" : "primary"}
                    >
                      {edit ? <DoDisturb /> : <ModeEdit />}
                    </IconButton>
                  </Tooltip>
                </>
              }
            >
              <Formik
                initialValues={row}
                validationSchema={Yup.object().shape({
                  firstName: Yup.string()
                    .uppercase("Solo se admiten mayusculas")
                    .required("Primer nombre es un campo obligatorio")
                    .strict(),
                  surname: Yup.string()
                    .uppercase("Solo se admiten mayusculas")
                    .required("Primer apellido es un campo obligatorio")
                    .strict(),
                  area: Yup.string(),
                })}
                onSubmit={(values, {}) => {}}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  isSubmitting,
                  touched,
                  values,
                }) => (
                  <form noValidate onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.surname && errors.surname)}
                        >
                          <InputLabel htmlFor="outlined-adornment-surname">
                            Primer Apellido
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-surname"
                            type="text"
                            value={values.surname}
                            name="surname"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Primer Apellido"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                            inputComponent={NamesMask}
                          />
                          {touched.surname && errors.surname && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-surname"
                            >
                              {errors.surname}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(
                            touched.secondSurname && errors.secondSurname
                          )}
                        >
                          <InputLabel htmlFor="outlined-adornment-secondSurname">
                            Segundo Apellido
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-secondSurname"
                            type="text"
                            value={values.secondSurname}
                            name="secondSurname"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Primer Apellido"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                          />
                          {touched.secondSurname && errors.secondSurname && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-secondSurname"
                            >
                              {errors.secondSurname}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.firstName && errors.firstName)}
                        >
                          <InputLabel htmlFor="outlined-adornment-firstName">
                            Primer Nombre
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-firstName"
                            type="text"
                            value={values.firstName}
                            name="firstName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Primer Apellido"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                          />
                          {touched.firstName && errors.firstName && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-firstName"
                            >
                              {errors.firstName}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.otherName && errors.otherName)}
                        >
                          <InputLabel htmlFor="outlined-adornment-otherName">
                            Otros Nombres
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-otherName"
                            type="text"
                            value={values.otherName}
                            name="otherName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Primer Apellido"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                          />
                          {touched.otherName && errors.otherName && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-otherName"
                            >
                              {errors.otherName}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.country && errors.country)}
                        >
                          <InputLabel id="simple-select-country-label">
                            Pais de empleo
                          </InputLabel>
                          <Select
                            labelId="simple-select-country-label"
                            id="simple-select-country"
                            value={values.country}
                            label="Pais de empleo"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="country"
                            disabled={!edit}
                          >
                            <MenuItem value="co">Colombia</MenuItem>
                            <MenuItem value="us">Estado Unidos</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.country && errors.country)}
                        >
                          <InputLabel id="simple-select-idType-label">
                            Tipo de Identificacion
                          </InputLabel>
                          <Select
                            labelId="simple-select-idType-label"
                            id="simple-select-idType"
                            value={values.idType}
                            label="Tipo de Identificacion"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="idType"
                            disabled={!edit}
                          >
                            <MenuItem value="cedula de ciudadania">
                              Cédula de Ciudadanía
                            </MenuItem>
                            <MenuItem value="cedula de extranjeria">
                              Cédula de Extranjería
                            </MenuItem>
                            <MenuItem value="pasaporte">Pasaporte</MenuItem>
                            <MenuItem value="permiso especial">
                              Permiso Especial
                            </MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.otherName && errors.otherName)}
                        >
                          <InputLabel htmlFor="outlined-adornment-id">
                            Numero de identificacion
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-id"
                            type="text"
                            value={values.id}
                            name="id"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Numero de identificacion"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                          />
                          {touched.id && errors.id && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-otherName"
                            >
                              {errors.id}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.otherName && errors.otherName)}
                        >
                          <InputLabel htmlFor="outlined-adornment-email">
                            Correo Electronico
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-email"
                            type="email"
                            value={values.email}
                            name="email"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Correo Electronico"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={true}
                          />
                          {touched.email && errors.email && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-otherName"
                            >
                              {errors.email}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item xs={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(
                            touched.dateAdmission && errors.dateAdmission
                          )}
                        >
                          <InputLabel htmlFor="outlined-adornment-dateAdmission">
                            Fecha de ingreso
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-dateAdmission"
                            type="text"
                            value={values.dateAdmission}
                            name="dateAdmission"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label="Fecha de ingreso"
                            inputProps={{}}
                            autoComplete="off"
                            disabled={!edit}
                            inputComponent={DateMask}
                          />
                          {touched.dateAdmission && errors.dateAdmission && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-dateAdmission"
                            >
                              {errors.dateAdmission}
                            </FormHelperText>
                          )}
                        </FormControl>
                      </Grid>
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.area && errors.area)}
                        >
                          <InputLabel id="simple-select-area-label">
                            Area
                          </InputLabel>
                          <Select
                            labelId="simple-select-area-label"
                            id="simple-select-area"
                            value={values.area}
                            label="Area"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="area"
                            disabled={!edit}
                          >
                            <MenuItem value="administracion">
                              Administracion
                            </MenuItem>
                            <MenuItem value="financiera">Financiera</MenuItem>
                            <MenuItem value="compras">Compras</MenuItem>
                            <MenuItem value="infraestructura">
                              Infraestructura
                            </MenuItem>
                            <MenuItem value="operacion">Operacion</MenuItem>
                            <MenuItem value="talento humano">
                              Talento Humano
                            </MenuItem>
                            <MenuItem value="servicios varios">
                              Servicios Varios
                            </MenuItem>
                            <MenuItem value="otros">Otros</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      {values.area === "otros" && (
                        <Grid item xs={6}>
                          <FormControl
                            fullWidth
                            error={Boolean(
                              touched.otherArea && errors.otherArea
                            )}
                          >
                            <InputLabel htmlFor="outlined-adornment-otherArea">
                              Otra area
                            </InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-otherArea"
                              type="text"
                              value={values.otherArea}
                              name="otherArea"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              label="Fecha de ingreso"
                              inputProps={{}}
                              autoComplete="off"
                              disabled={!edit}
                            />
                            {touched.otherArea && errors.otherArea && (
                              <FormHelperText
                                error
                                id="standard-weight-helper-text-otherArea"
                              >
                                {errors.otherArea}
                              </FormHelperText>
                            )}
                          </FormControl>
                        </Grid>
                      )}
                      <Grid item md={6}>
                        <FormControl
                          fullWidth
                          error={Boolean(touched.state && errors.state)}
                        >
                          <InputLabel id="simple-select-state-label">
                            Estado
                          </InputLabel>
                          <Select
                            labelId="simple-select-state-label"
                            id="simple-select-state"
                            value={values.state}
                            label="Estado"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="state"
                            disabled={!edit}
                          >
                            <MenuItem value="activo">Activo</MenuItem>
                            <MenuItem value="inactivo">Inactivo</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                    </Grid>
                  </form>
                )}
              </Formik>
            </MainCard>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

export default Row;
