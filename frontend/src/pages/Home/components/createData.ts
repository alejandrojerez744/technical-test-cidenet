export function createData(
  surname: string,
  secondSurname: string,
  firstName: string,
  otherName: string,
  country: string,
  idType: string,
  id: string,
  dateAdmission: string,
  area: string,
  otherArea: string,
  state: string,
  email: string
) {
  return {
    surname,
    secondSurname,
    firstName,
    otherName,
    country,
    idType,
    id,
    dateAdmission,
    area,
    otherArea,
    state,
    email,
  };
}
