import { any } from "prop-types";
import { forwardRef } from "react";
import { IMaskInput, IMask } from "react-imask";

export const NamesMask = forwardRef((props: any, ref: any) => {
  const { onChange, ...other } = props;
  return (
    <IMaskInput
      {...other}
      mask={/^[A-Z]{1,20}$/}
      inputRef={ref}
      onAccept={(value: any) =>
        onChange({ target: { name: props.name, value } })
      }
      overwrite
    />
  );
});

export const DateMask = forwardRef((props: any, ref: any) => {
  const { onChange, ...other } = props;
  return (
    <IMaskInput
      {...other}
      mask="00/00/0000"
      inputRef={ref}
      onAccept={(value: any) =>
        onChange({ target: { name: props.name, value } })
      }
      overwrite
    />
  );
});
