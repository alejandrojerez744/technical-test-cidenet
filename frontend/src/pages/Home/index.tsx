import {
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { useState } from "react";
import { createData } from "./components/createData";
import Row from "./components/Row";

const Home = () => {
  const [rows, setRows] = useState([
    createData(
      "JEREZ",
      "GIRALDO",
      "DIEGO",
      "ALEJANDRO",
      "co",
      "cedula de ciudadania",
      "1003710356",
      "01/01/2022",
      "administracion",
      "",
      "activo",
      "JEREZ.DIEGO@cedinet.com.co"
    ),
  ]);
  return (
    <Box sx={{ m: 5 }}>
      <TableContainer component={Paper}>
        <Table aria-label="callapsible table employes">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>Primer Nombre</TableCell>
              <TableCell>Primer Apellido</TableCell>
              <TableCell>Area</TableCell>
              <TableCell>Estado</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <Row row={row} key={row.firstName} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Home;
