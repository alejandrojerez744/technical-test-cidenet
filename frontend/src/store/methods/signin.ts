import axios from "axios";

export default (email: string, password: string) => {
  return axios({
    url: "http://localhost:3001/api/auth/sign-in",
    method: "POST",
    auth: { username: email, password },
  })
    .then((res) => res)
    .catch((error) => error);
};
