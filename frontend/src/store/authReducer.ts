import { AUTH_SIGN_IN } from "./actions";
import decode from "jwt-decode";

const initialState = {
  user: null,
};

const authReducer = (state = initialState, action: any) => {
  const dispatchers: { [key: string]: object } = {
    [AUTH_SIGN_IN]: {
      ...state,
      user: action.user ? decode(action.user) : null,
    },
  };

  return dispatchers[action.type] || state;
};

export default authReducer;
