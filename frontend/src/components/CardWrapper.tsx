import { Box } from "@mui/system";
import { node } from "prop-types";
import MainCard from "./MainCard";

type Props = any;

const CardWrapper: React.FC<Props> = ({ children, ...other }) => {
  return (
    <MainCard
      sx={{
        maxWidth: { xs: other.widthxs, lg: other.widthlg },
        margin: { xs: 2.5, md: 3 },
        "& > *": {
          flexGrow: 1,
          flexBasis: "50%",
        },
      }}
      content={false}
      {...other}
    >
      <Box sx={{ p: { xs: 2, sm: 5, xl: 5 } }}>{children}</Box>
    </MainCard>
  );
};

CardWrapper.propTypes = {
  children: node,
};

export default CardWrapper;
