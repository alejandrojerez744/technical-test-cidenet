import {
  Grid,
  Box,
  Typography,
  FormControl,
  InputLabel,
  OutlinedInput,
  FormHelperText,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { Formik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import AnimateButton from "./AnimateButton";
import signin from "../store/methods/signin";
import { useDispatch } from "react-redux";
import { AUTH_SIGN_IN } from "../store/actions";

const AuthLogin = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };

  return (
    <>
      <Grid container direction="column" justifyContent="center" spacing={2}>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justifyContent="center"
        >
          <Box sx={{ mb: 2 }}>
            <Typography variant="subtitle1">
              Sign in with Email address
            </Typography>
          </Box>
        </Grid>
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Must be a valid email")
              .max(255)
              .required("Email is required"),
            password: Yup.string()
              .max(20)
              .min(6)
              .required("Password is required"),
          })}
          onSubmit={(values, { setFieldError, setSubmitting }) => {
            const { email, password } = values;
            signin(email, password)
              .then((res) => {
                setSubmitting(false);
                if (res?.data?.data.token) {
                  console.log(res?.data?.data.token);
                  dispatch({
                    type: AUTH_SIGN_IN,
                    action: res?.data?.data.token,
                  });
                  setTimeout(() => {
                    navigate("/home");
                  }, 1000);
                } else {
                  setFieldError("email", "correo electronico invalido");
                  setFieldError("password", "contraseña invalida");
                }
              })
              .catch((error) => {
                console.log("error", error);
                setSubmitting(false);
              });
          }}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
            status,
          }) => (
            <form noValidate onSubmit={handleSubmit}>
              <FormControl
                fullWidth
                error={Boolean(touched.email && errors.email)}
              >
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Email Address
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="email"
                  value={values.email}
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  label="Email Address"
                  inputProps={{}}
                  autoComplete="off"
                />
              </FormControl>
              {touched.email && errors.email && (
                <FormHelperText
                  error
                  id="standard-weight-helper-text-email-login"
                >
                  {errors.email}
                </FormHelperText>
              )}
              <FormControl
                fullWidth
                error={Boolean(touched.password && errors.password)}
                sx={{ mt: 2 }}
              >
                <InputLabel htmlFor="outlined-adornment-password-login">
                  Password
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password-login"
                  type={showPassword ? "text" : "password"}
                  value={values.password}
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                        size="large"
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                  inputProps={{}}
                  autoComplete="off"
                />
                {touched.password && errors.password && (
                  <FormHelperText
                    error
                    id="standard-weight-helper-text-password-login"
                  >
                    {errors.password}
                  </FormHelperText>
                )}
              </FormControl>
              <Box sx={{ mt: 2 }}>
                <AnimateButton>
                  <Button
                    disableElevation
                    disabled={isSubmitting}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                    color={status}
                  >
                    Sign in
                  </Button>
                </AnimateButton>
              </Box>
            </form>
          )}
        </Formik>
      </Grid>
    </>
  );
};

export default AuthLogin;
