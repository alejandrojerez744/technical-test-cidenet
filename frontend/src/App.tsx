import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline, StyledEngineProvider } from "@mui/material";
import NavigationScroll from "./layout/NavigationScroll";
import Routes from "./routes";

export const App = () => {
  return (
    <StyledEngineProvider injectFirst>
      <CssBaseline />
      <NavigationScroll>
        <Routes />
      </NavigationScroll>
    </StyledEngineProvider>
  );
};
