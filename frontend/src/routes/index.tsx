import { useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router-dom";
import BasicLayout from "../layout/Basic";
import HomeLayout from "../layout/Home";
import Home from "../pages/Home";
import SignIn from "../pages/SignIn";

export default () => {
  const { user } = useSelector((state: any) => state.auth);

  return (
    <Routes>
      <Route path="/" element={<BasicLayout />}>
        <Route index element={user ? <Navigate to="/home" /> : <SignIn />} />
      </Route>
      <Route path="/home" element={<HomeLayout />}>
        <Route index element={user ? <Home /> : <Navigate to="/" />} />
      </Route>
    </Routes>
  );
};
